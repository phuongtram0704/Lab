# variables.tf

variable "vpc_id" {
  description = "The ID of the VPC where the resources will be created"
}

variable "key_name" {
  description = "The name of the SSH key pair to use for EC2 instance"
}


variable "eip_name" {
  description = "Name for the EIP resource"
  default     = "example"
}

variable "internet_gateway_name" {
  description = "Name for the internet gateway resource"
  default     = "example"
}