# main.tf

# Configure AWS provider
provider "aws" {
  region = "us-east-1"  # US region
  access_key = "AKIAQ3EGPHIKAZL4IF4N"
  secret_key = "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"
} 

# Allocate Elastic IP address
resource "aws_eip" "eip" {
  instance = aws_instance.lab.id
}

# Create internet gateway
resource "aws_internet_gateway" "demo-gw" {
  vpc_id = var.vpc_id  # Replace with your VPC ID
}


# Create security group allowing SSH and HTTP access
resource "aws_security_group" "lab" {
  name        = "web_security_group"
  description = "Allow SSH and HTTP inbound traffic"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Launch EC2 instance
resource "aws_instance" "lab" {
  ami                    = "ami-080e1f13689e07408"  # Ubuntu 20.04 LTS AMI ID
  instance_type          = "t2.micro"
  key_name               = var.key_name   # Replace with your key pair name
  security_groups        = [aws_security_group.lab.name] # Reference the security group name directly

  tags = {
    Name = "lab-instance"
  }
}